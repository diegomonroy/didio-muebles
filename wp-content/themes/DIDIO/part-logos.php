<!-- Begin Logos -->
	<section class="logos wow fadeIn" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<h3>Nuestros Clientes</h3>
				<?php echo do_shortcode( '[tc-owl-carousel carousel_cat="nuestros-clientes" order="ASC"]' ); ?>
			</div>
		</div>
	</section>
<!-- End Logos -->