<?php if ( is_front_page() ) : ?>
<!-- Begin Gallery -->
	<section class="gallery wow fadeIn" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'gallery' ); ?>
			</div>
		</div>
	</section>
<!-- End Gallery -->
<?php endif; ?>