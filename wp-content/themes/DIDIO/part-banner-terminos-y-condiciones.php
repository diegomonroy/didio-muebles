<!-- Begin Banner -->
	<section class="banner wow fadeIn" data-wow-delay="0.5s">
		<div class="expanded row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'banner_terms_and_conditions' ); ?>
			</div>
		</div>
	</section>
<!-- End Banner -->