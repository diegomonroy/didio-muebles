// JavaScript Document

/* ************************************************************************************************************************

DIDIO muebles

File:			app.js
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2017

************************************************************************************************************************ */

/* Foundation */

$(document).foundation();

/* WOW */

new WOW().init();

/* jQuery */

jQuery.noConflict();

jQuery(document).ready(function () {
	/* Content */
	jQuery( 'ul.sub-menu' ).addClass( 'menu' );
	jQuery( '.tnp.tnp-widget' ).addClass( 'columns' );
	var kids = 'DIDIO <span class="color_1">K</span><span class="color_2">I</span><span class="color_3">D</span><span class="color_4">S</span>';
	jQuery( 'a.kids' ).html( kids );
	jQuery( 'li.cat-item-46 a' ).html( kids );
	/* Search */
	var searchBox = jQuery( '.moduletable_s1' );
	var searchBoxVar = searchBox.css( 'display' );
	var searchButton = jQuery( '#search_button' );
	var searchClose = jQuery( '#search_close' );
	if ( searchBoxVar == 'none' ) {
		searchButton.on('click mouseover', function ( event ) {
			event.preventDefault();
			searchBox.css( 'display', 'block' ).animate( { width: '250px', opacity: 1 }, 1000 );
			searchButton.css( 'display', 'none' );
			searchClose.css( 'display', 'inline' );
		});
	}
	searchClose.on('click', function ( event ) {
		event.preventDefault();
		searchBox.css( 'display', 'none' ).animate( { width: '0', opacity: 0 }, 1000 );
		searchButton.css( 'display', 'inline' );
		searchClose.css( 'display', 'none' );
	});
	/* Newsletter */
	jQuery( '.tnp-email' ).attr( 'placeholder', 'Ingresa tu correo' );
	jQuery( 'input.tnp-submit' ).prop( 'value', 'ENVIAR' );
});

/* Back To Top */

if ( jQuery( '#back-to-top' ).length ) {
	var scrollTrigger = 100,
	backToTop = function () {
		var scrollTop = jQuery( window ).scrollTop();
		if ( scrollTop > scrollTrigger ) {
			jQuery( '#back-to-top' ).addClass( 'show' );
		} else {
			jQuery( '#back-to-top' ).removeClass( 'show' );
		}
	};
	backToTop();
	jQuery( window ).on( 'scroll', function () {
		backToTop();
	});
	jQuery( '#back-to-top' ).on( 'click', function ( e ) {
		e.preventDefault();
		jQuery( 'html, body' ).animate({
			scrollTop: 0
		}, 700);
	});
}