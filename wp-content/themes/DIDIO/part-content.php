<?php
if ( is_page( 'inicio' ) ) :
	get_template_part( 'part', 'banner-home' );
endif;
if ( is_page( 'nosotros' ) ) :
	get_template_part( 'part', 'banner-about-us' );
endif;
if ( is_page( 'servicios' ) ) :
	get_template_part( 'part', 'banner-services' );
endif;
if ( is_page( 'contacto' ) ) :
	get_template_part( 'part', 'banner-contact' );
endif;
if ( is_page( 'terminos-y-condiciones' ) ) :
	get_template_part( 'part', 'banner-terminos-y-condiciones' );
endif;
?>
<!-- Begin Content -->
	<section class="content wow fadeIn" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</section>
<!-- End Content -->