<!-- Begin WooCommerce -->
	<section class="woocommerce wow fadeIn" data-wow-delay="0.5s">
		<div class="show-for-small-only">
			<button type="button" class="button" data-toggle="offCanvas">Catálogo</button>
			<div class="off-canvas-wrapper">
				<div class="off-canvas-absolute position-left" id="offCanvas" data-off-canvas>
					<button type="button" class="close-button" aria-label="Cerrar" data-close><span aria-hidden="true">&times;</span></button>
					<?php dynamic_sidebar( 'left_menu' ); ?>
					<?php dynamic_sidebar( 'left_banner' ); ?>
				</div>
				<div class="off-canvas-content" data-off-canvas-content>
					<div class="row">
						<div class="small-12 columns">
							<?php woocommerce_content(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="hide-for-small-only">
			<div class="row">
				<div class="medium-3 columns">
					<?php dynamic_sidebar( 'left_menu' ); ?>
					<?php dynamic_sidebar( 'left_banner' ); ?>
				</div>
				<div class="medium-9 columns">
					<?php woocommerce_content(); ?>
				</div>
			</div>
		</div>
	</section>
<!-- End WooCommerce -->