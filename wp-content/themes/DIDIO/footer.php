		<?php get_template_part( 'part', 'featured-products' ); ?>
		<?php get_template_part( 'part', 'gallery' ); ?>
		<?php get_template_part( 'part', 'banner-bottom' ); ?>
		<?php get_template_part( 'part', 'logos' ); ?>
		<?php get_template_part( 'part', 'newsletter' ); ?>
		<?php get_template_part( 'part', 'bottom' ); ?>
		<?php get_template_part( 'part', 'copyright' ); ?>
		<!-- Begin Back To Top -->
			<a href="#" id="back-to-top" title="Ir Arriba">Ir Arriba</a>
		<!-- End Back To Top -->
		<?php wp_footer(); ?>
	</body>
</html>