<!-- Begin Featured Products -->
	<section class="featured_products wow fadeIn" data-wow-delay="0.5s">
		<h3>Productos Destacados</h3>
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'featured_products' ); ?>
			</div>
		</div>
	</section>
<!-- End Featured Products -->