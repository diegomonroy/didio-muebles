<!-- Begin Menu -->
	<section class="menu_wrap wow fadeIn" data-wow-delay="0.5s">
		<div class="row align-center align-middle">
			<div class="small-4 columns show-for-small-only header_logo">
				<!--<a href="/"><img src="<?php echo get_template_directory_uri(); ?>/build/logo.png" title="<?php bloginfo(title); ?>" alt="<?php bloginfo(title); ?>"></a>-->
				<a href="/">Muebles DIDIO</a>
			</div>
			<div class="small-4 medium-10 columns">
				<div class="hide-for-small-only">
					<?php dynamic_sidebar( 'menu' ); ?>
				</div>
				<!--<div class="moduletable_m1">
					<div class="top-bar">
						<div class="top-bar-title">
							<span data-responsive-toggle="responsive-menu" data-hide-for="medium">
								<button class="menu-icon" type="button" data-toggle></button>
								<strong>Menú</strong>
							</span>
						</div>
						<div id="responsive-menu">
							<div class="top-bar-left">
								<?php
								wp_nav_menu(
									array(
										'menu_class' => 'dropdown menu',
										'container' => false,
										'theme_location' => 'main-menu',
										'items_wrap' => '<ul class="%2$s" data-dropdown-menu>%3$s</ul>'
									)
								);
								?>
							</div>
						</div>
					</div>
				</div>-->
			</div>
			<div class="small-4 medium-2 columns text-center">
				<?php dynamic_sidebar( 'social_media' ); ?>
				<?php dynamic_sidebar( 'search' ); ?>
			</div>
		</div>
		<div class="row align-center align-middle show-for-small-only">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'menu' ); ?>
			</div>
		</div>
	</section>
<!-- End Menu -->