<!-- Begin Top -->
	<section class="top wow fadeIn" data-wow-delay="0.5s">
		<div class="row align-middle">
			<div class="small-6 columns text-left">
				<?php if ( is_user_logged_in() ) { ?>
				<span class="hide-for-small-only">Bienvenido/a! </span><a href="<?php echo get_page_link( 8 ); ?>">Mi Cuenta</a>
				<?php } else { ?>
				<span class="hide-for-small-only">Bienvenido/a! </span><a href="<?php echo get_page_link( 8 ); ?>">Inicia sesión o Regístrate aquí</a>
				<?php } ?>
			</div>
			<div class="small-6 columns text-right">
				<a href="<?php echo get_page_link( 6 ); ?>">Carrito de Compras <i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
			</div>
		</div>
	</section>
<!-- End Top -->
<!-- Begin Header Logo -->
	<!--<section class="show-for-small-only header_logo">
		<a href="/"><img src="<?php echo get_template_directory_uri(); ?>/build/logo.png" title="<?php bloginfo(title); ?>" alt="<?php bloginfo(title); ?>"></a>
	</section>-->
<!-- End Header Logo -->
<?php get_template_part( 'part', 'menu' ); ?>