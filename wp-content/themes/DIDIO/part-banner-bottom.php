<!-- Begin Banner Bottom -->
	<section class="banner_bottom wow fadeIn" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'banner_bottom' ); ?>
			</div>
		</div>
	</section>
<!-- End Banner Bottom -->